# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This script is to form a dependency graph for mysql database, remotely dump, concatenate and format into H2 compatible data scripts.

### How do I get set up? ###
	requires node v7 above for async await
	Steps:
	npm install
	node --harmony h2gen.js
	sed "s/[\]['][,]/\\\ ',/g;s/[\]['][,]/\ ',/g;s/[\][']/''/g;s/'\\\0'/0/g;s/'\\\1'/1/g;s/\`//g" h2dump-tc/final.sql >> h2dump-tc/formattedfinal.sql
	Replace '\u0001' to 1 in h2dump-tc/formattedfinal.sql

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact