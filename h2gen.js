const shell = require('shelljs');
const mysql = require('promise-mysql');
const _ = require('lodash');

const credentials = {
    host: "tc-prod.cluster-cmfydxrklw15.us-east-1.rds.amazonaws.com",
    user: "tcuser",
    password: "TheGreatGatsby!1"
};
// const credentials = {
//     host: "tradingcentre-staging-migrated.cmfydxrklw15.us-east-1.rds.amazonaws.com",
//     user: "manage_dbuser",
//     password: "Test123Manage"
// };

const folder = "h2dump-tc";

const outputFile = `${folder}/final.sql`;

const dumpStr = `mysqldump -h${credentials.host} -u${credentials.user} -p${credentials.password} --no-create-info --skip-triggers --compact`;

async function dump(database, table, whereClause) {
    const fileName = `${folder}/${database}_${table}.sql`;
    let dump;
    const {key, ids} = whereClause;
    if (whereClause) {
        if (ids.length > 0)
            dump = `${dumpStr}  ${database} ${table} --where="${key} in (${ids.join(',')}) " > ${fileName}`;
    }
    else
        dump = `${dumpStr}  ${database} ${table} > ${fileName}`;
    // console.log(dump);
    if (ids.length > 0) {
        shell.exec(dump);
        const command = 'sed ' + '"' + "s/INTO /INTO " + database + "./g" + '" ' + fileName + " >> " + outputFile;
        shell.exec(command);
        shell.exec(`sleep 5`);
    }
}

//dump('trading_center','campaign');
function Table(schema, name) {
    this.schema = schema;
    this.name = name;
    this.references = [];
}

Table.prototype.addReference = function (table) {
    this.references.push(table);
}

function TableStore() {
    this.tables = {};
}

TableStore.prototype.get = function (database, table) {
    return this.tables[database + '-' + table];
}

TableStore.prototype.set = function (database, table) {
    return this.tables[database + '-' + table] = new Table(database, table);
}

function findDependency(database, table, seen) {
    if (!table) {
        const tablesOfThisdatabase = allTables.filter(table => table.org_database === database);
        // console.log(tablesOfThisdatabase);
        tablesOfThisdatabase.forEach(tab => {
            const {org_database: database, org_table: table} = tab;
            // console.log(database,table);
            findDependency(database, table, []);
        });
    } else {
        seen.push(database + '-' + table)
        const deps = depList.filter(res => res.org_database === database && res.org_table === table);
        // console.log(deps);
        deps.forEach((dep, i) => {
            const {ref_database: database, ref_table: table} = dep;
            // console.log(database,table);
            if (resolved.indexOf(database + '-' + table) === -1)
                if (seen.indexOf(database + '-' + table) > -1) {
                    console.log('Warning: Circular reference detected, ignore the resolution:', database, table);
                }
                else
                    findDependency(database, table, seen);
        });
        resolved.push(database + '-' + table);
    }
}

let allTables = [];
let depList = [];
let resolved = [];

async function tableDepList(database, table) {
    const connection = await mysql.createConnection(credentials);
    const results = await connection.query(`SELECT TABLE_NAME as org_table, TABLE_SCHEMA as org_database, REFERENCED_TABLE_NAME as ref_table, REFERENCED_TABLE_SCHEMA as ref_database from information_schema.KEY_COLUMN_USAGE`);
    const results2 = await connection.query(`SELECT TABLE_NAME as org_table,TABLE_SCHEMA as org_database from information_schema.TABLES where TABLE_TYPE = 'BASE TABLE'`);
    allTables = results2;
    depList = _.uniqWith(results, _.isEqual)
        .filter(result => result.ref_table != null && result.ref_database != null)
        .filter(result => result.org_table != result.ref_table && result.org_database != result.ref_database);
    findDependency('miq', null, []);
    resolved = _.uniq(resolved);
    console.log(resolved);
    shell.rm('-rf', folder);
    shell.mkdir(folder);
    resolved.forEach(tableName => {
        const [database, table] = tableName.split('-');
        // dump(database,table);
    });
    connection.destroy();
    try {
        getTradingCenterData(3076);
    }
    catch (error) {
        console.log(error.message);
    }

}

async function getTradingCenterData(...campaignIds) {
    const connection = await mysql.createConnection(credentials);
    console.log(connection);
    try {


        let io_ids = await connection.query(`SELECT io_id from trading_center.campaign_io_association where campaign_id in (${campaignIds.join(',')})`);
        io_ids = io_ids.map(row => row.io_id);
        console.log(io_ids);
        var placement_ids = [];

        if (io_ids.length > 0)
            placement_ids = await connection.query(`SELECT placement_id from trading_center.daily_io_placement_association_with_date_range where io_id in (${io_ids.join(',')})`);


        placement_ids = placement_ids.map(row => row.placement_id);
        let pacing_ids = await connection.query(`SELECT pacing_id from trading_center.campaign_has_pacing where campaign_id in (${campaignIds.join(',')})`);
        pacing_ids = pacing_ids.map(row => row.pacing_id);
        console.log(campaignIds, io_ids, placement_ids, pacing_ids);
        dump('trading_center', 'campaign', {key: 'id', ids: campaignIds});
        dump('trading_center', 'campaign_booking_allocations_tmp2', {key: 'campaign_id', ids: campaignIds});
        dump('trading_center', 'campaign_daily_margin', {key: 'campaign_id', ids: campaignIds});
        dump('trading_center', 'campaign_daily_pixel', {key: 'campaign_id', ids: campaignIds});
        dump('trading_center', 'campaign_daily_placement_client_stats', {key: 'campaign_id', ids: campaignIds});
        dump('trading_center', 'campaign_daily_placement_conversion_stats', {key: 'campaign_id', ids: campaignIds});
        dump('trading_center', 'campaign_elevate_association', {key: 'campaign_id', ids: campaignIds});
        dump('trading_center', 'campaign_goals', {key: 'campaign_id', ids: campaignIds});
        dump('trading_center', 'campaign_has_countries', {key: 'campaign_id', ids: campaignIds});
        dump('trading_center', 'pacing', {key: 'id', ids: pacing_ids});
        dump('trading_center', 'campaign_has_pacing', {key: 'campaign_id', ids: campaignIds});
        dump('trading_center', 'campaign_io_association', {key: 'campaign_id', ids: campaignIds});
        dump('trading_center', 'campaign_kpi_pixel', {key: 'campaign_id', ids: campaignIds});
        dump('trading_center', 'campaign_lifetime_info', {key: 'id', ids: campaignIds});
        dump('trading_center', 'campaign_monthly_info', {key: 'id', ids: campaignIds});
        dump('trading_center', 'campaign_monthly_status', {key: 'campaign_id', ids: campaignIds});
        dump('trading_center', 'campaign_reported_pacing_intervals', {key: 'campaign_id', ids: campaignIds});
        dump('trading_center', 'client_creative_daily_stats', {key: 'campaign_id', ids: campaignIds});
        dump('trading_center', 'client_creative_to_dsp_creative_association', {key: 'campaign_id', ids: campaignIds});
        dump('trading_center', 'creative_tag', {key: 'campaign_id', ids: campaignIds});
        dump('trading_center', 'daily_insertion_order_stats', {key: 'insertion_order_id', ids: io_ids});

        dump('trading_center', 'daily_io_placement_association_with_date_range', {key: 'io_id', ids: io_ids});


        dump('trading_center', 'daily_pixel_stats', {key: 'placement_id', ids: placement_ids});
        dump('trading_center', 'daily_placement_stats', {key: 'placement_id', ids: placement_ids});
        dump('trading_center', 'daily_seller_cost', {key: 'placement_id', ids: placement_ids});
        dump('trading_center', 'dashboard_stats', {key: 'io_id', ids: io_ids});
        dump('trading_center', 'deduped_creative_daily_stats', {key: 'campaign_id', ids: campaignIds});
        dump('trading_center', 'dsp_creative_lookup', {key: 'campaign_id', ids: campaignIds});
        dump('trading_center', 'dsp_pixels');
        dump('trading_center', 'dynamic_data_provider_daily_cost', {key: 'placement_id', ids: placement_ids});
        dump('trading_center', 'static_data_provider_daily_cost', {key: 'placement_id', ids: placement_ids});
        dump('trading_center', 'third_party_pixel_cost', {key: 'placement_id', ids: placement_ids});

    }
    catch (error) {
        console.log(error.message);
    }
    connection.destroy();
}

tableDepList();
